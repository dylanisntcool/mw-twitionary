//
// MW Twitionary Runtime
//
var Bot = require("./bot"), config = require("./config");
var Twit = require('../node_modules/twit/lib/twitter');
var bot = new Bot(config);
var dictionaryKey = "2d0171e5-dc37-4e5c-89f3-5a9e5db8ae83";

//get date string for today's date (e.g. 2011-01-01)
function datestring () {
  var d = new Date(Date.now() - 5*60*60*1000); //est timezone
  return d.getUTCFullYear() + "-" + (d.getUTCMonth() + 1) + "-" + d.getDate();
};

console.log('\nMW Twitionary has been fired up, prepare the nerds.');
console.log('MW Key: ' + dictionaryKey );
console.log('MW Date: ' + datestring () );
bot.twit.get("followers/ids", function(err, reply) {
	if(err) return handleError(err)
	console.log("# Followers:" + reply.ids.length.toString());
});

var stream = bot.twit.stream('statuses/filter', { track: '#twitionary', language: 'en' });

stream.on('tweet', function (tweet) {
  console.log( "\n" + tweet.text );
  console.log( tweet.user.screen_name );
  var appResponse = "@" + tweet.user.screen_name + " this is the twitionary, definitions coming soon";
  bot.tweet(appResponse, function (err, reply) {
    if(err) return handleError(err);
    console.log('\nTweet: ' + (reply ? reply.text : reply));
  })
})

//
// Handle Errors
//

function handleError(err) {
  console.error("response status:", err.statusCode);
  console.error("data:", err.data);
}